require 'spec_helper'

require 'spec_helper'

describe UploadFileDecorator do
  let(:upload_file) { FactoryGirl.create(:upload_file).decorate }

  subject { upload_file }

  describe ".image?" do
    it 'return true if attach image' do
      upload_file.image?.should eq(true)
    end
  end

  describe ".audio?" do
    it 'return true if attach audio' do
      upload_file.audio?.should eq(false)
    end
  end

end