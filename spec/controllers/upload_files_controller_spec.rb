require 'rails_helper'

describe UploadFilesController do

  let(:upload_file) { FactoryGirl.create(:upload_file, user: @user) }

  describe "GET index" do

    subject { response }
    before { get :index }
    let (:template) { 'upload_files/index' }

    it { should have_http_status(200) }
    it { should render_template(template) }
    it { expect(response.content_type).to eq 'text/html' }
  end

  describe 'POST #create' do
    it 'creates upload file' do
      upload_file_params = FactoryGirl.attributes_for(:upload_file)
      expect { post :create, upload_file: upload_file_params }.to change(UploadFile, :count).by(1)
    end
  end

  describe 'GET #show' do
    it 'render template show' do
      get :show, id: upload_file
      response.should render_template :show
    end
  end

  describe 'POST #destroy' do
    it 'destroys upload file' do
      expect { post :destroy, id: upload_file.id }.to change(UploadFile, :count).by(1)
    end
  end

  describe 'GET #download' do
    it "should return attachment" do
      expect(@controller).to receive(:send_file) {
        @controller.render nothing: true # to prevent a 'missing template' error
      }
      get :download, id: upload_file.id
    end
  end
end