require 'rails_helper'

describe DebugController do

  context "for signet users" do

    before { login_user }

    describe "GET delete_all_files" do

      subject { response }
      before { get :delete_all_files }
      let (:template) { 'upload_files/index' }
      it { should have_http_status(302) }
      it { expect(response.content_type).to eq 'text/html' }

      it 'delete all upload files' do
        expect { response }.to change { UploadFile.count }.by(0)
      end
    end


    describe "GET delete_all_users" do

      subject { response }
      before { get :delete_all_users }
      let (:template) { 'upload_files/index' }
      it { should have_http_status(302) }
      it { expect(response.content_type).to eq 'text/html' }

      it 'delete all upload files' do
        expect { response }.to change { User.count }.by(0)
      end
    end

  end
end