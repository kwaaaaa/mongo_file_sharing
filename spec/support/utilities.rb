def full_title(page_title)
  base_title = "MongoShara"
  if page_title.empty?
    base_title
  else
    "#{base_title} | #{page_title}"
  end
end

def login_user
  @request.env["devise.mapping"] = Devise.mappings[:user]
  user = FactoryGirl.create(:user, role: 'user')
  sign_in user
  user
end
