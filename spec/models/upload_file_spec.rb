  require 'spec_helper'

describe UploadFile do
  let(:upload_file) { FactoryGirl.create(:upload_file) }

  subject { upload_file }

  it { should respond_to(:content) }
  it { should respond_to(:attachment) }

  it { should be_valid }
  
  describe 'when content longer that 140 characters' do
    before { upload_file.content = "a" * 141 }
    it { should_not be_valid }
  end

end