require 'rails_helper'

describe 'should not raise exceptions' do
  it { expect{require "#{Rails.root}/db/seeds"}.to_not raise_exception }
end
