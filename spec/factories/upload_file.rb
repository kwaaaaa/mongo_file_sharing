FactoryGirl.define do
  factory :upload_file do
    content "Hello there!"
    attachment { fixture_file_upload(Rails.root.join('spec', 'images', 'test.png'), 'image/png') }
    user
  end
end