include ActionView::Helpers::NumberHelper

class UploadFileDecorator < Draper::Decorator
  decorates :upload_file

  def download_link
    h.link_to "download #{model.attachment_file_name}", "#{model.id}/download/", target: '_blank'
  end

  def size
    number_to_human_size(model.attachment.size)
  end

  def author_email
    if model.user.email.present?
        model.user.email
    else
      h.content_tag :span, 'anonim'
    end
  end

  def link_to_show
    h.link_to 'Show', "/#{model.id}", class: 'link-to-text-color'
  end

  def link_to_destroy
    h.link_to 'Delete', "/#{model.id}", method: :delete, data: { confirm: 'Are you sure?' }
  end

  def content
    "content: " + model.content if model.content.present?
  end

  def attach
    if self.image?
      h.content_tag(:div, class: 'image') do
        h.image_tag self.get_attach_file
      end
    elsif self.audio?
      h.audio_tag self.get_attach_file, controls: true, autoplay: false
    else
      download_link
    end
  end

  def get_attach_path
    model.attachment.path
  end

  def image?
    model.attachment_content_type.match(/image/) ? true : false
  end

  def audio?
    model.attachment_content_type.match(/audio/) ? true : false
  end

  def get_attach_file
    model.attachment.path.gsub("#{Rails.root}/public", "")
  end


end
