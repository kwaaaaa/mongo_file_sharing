class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_user_session_path
  end

  rescue_from Mongoid::Errors::DocumentNotFound do |exception|
    redirect_to new_user_registration_path
  end

  def current_user
    super || guest_user
  end

  def guest_user
    User.find(session[:guest_user_id].nil? ? session[:guest_user_id] = create_guest_user.id : session[:guest_user_id])
  end

  def create_guest_user
    user = User.new()
    user.save(validate: false)
    user
  end

end
