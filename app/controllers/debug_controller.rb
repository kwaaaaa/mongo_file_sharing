class DebugController < ApplicationController

  def delete_all_files
    UploadFile.delete_all
    redirect_to root_path
  end

  def delete_all_users
    User.delete_all
    redirect_to root_path
  end

end
