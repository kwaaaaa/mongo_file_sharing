 class UploadFilesController < ApplicationController
  load_and_authorize_resource
  before_action :set_upload_file, only: [:show, :download]

  def index
    @upload_files = UploadFileDecorator.decorate_collection(UploadFile.order('attachment_updated_at desc'))
    @upload_file = UploadFile.new
  end

  def create
    @upload_file = current_user.upload_files.build(upload_file_params)
    respond_to do |format|
      if @upload_file.save
        format.js
        format.html { redirect_to upload_files_path, notice: 'Upload file was successfully created.' }
      else
        format.js
        format.html { redirect_to upload_files_path, alert: @upload_file.errors.full_messages.join(", ") }
      end
    end
  end

  def show
  end

  def destroy
    @upload_file.destroy
    respond_to do |format|
      format.js
      format.html { redirect_to upload_files_url, notice: 'Upload file was successfully destroyed.' }
    end
  end

  def download
    send_file @upload_file.get_attach_path
  end

  private
    def set_upload_file
        @upload_file = UploadFile.find(params[:id]).decorate
    end

    def upload_file_params
      params.require(:upload_file).permit(:content, :attachment)
    end
end
