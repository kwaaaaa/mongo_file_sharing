$('#upload_file_attachment').fileupload({

  // replaceFileInput: false,

    progress: function (e, data) {
      var progress_var = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .bar').css('width', progress_var + '%');
    },

    done: function (e, data) {
      $('#new_upload_file > p:nth-child(6) > input:nth-child(1)').click();
    },

    fail: function(e, data) {
      alert('Fail!');
    }

});