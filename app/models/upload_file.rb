class UploadFile

  include Mongoid::Document
  include Mongoid::Paperclip

  belongs_to :user
  field :content, type: String, default: ""

  has_mongoid_attached_file :attachment,
   path: "#{Rails.root}/public/attachments/:id.:extension"

  
  validates :content, length: { maximum: 120 }

  validates :attachment, attachment_presence: true
  validates_with AttachmentSizeValidator, attributes: :attachment, less_than: 10.megabytes
  do_not_validate_attachment_file_type :attachment

end
