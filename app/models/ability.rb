class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all

    else user.user?
      can :read, :all
      can :create, UploadFile
      can :download, UploadFile
      can :destroy, UploadFile do |f|
        f.try(:user) == user
      end
    end
  end
end
