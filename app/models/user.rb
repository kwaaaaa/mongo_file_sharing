class User
  include Mongoid::Document
  has_many :upload_files

  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  ## Database authenticatable
  field :email,               type: String, default: ""
  field :encrypted_password,  type: String, default: ""

  ## Rememberable
  field :remember_created_at, type: Time
  field :role,                type: String, default: "user"

  def admin?
    role == 'admin'
  end

  def user?
    role == 'user'
  end


end
