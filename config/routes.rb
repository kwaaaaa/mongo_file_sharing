Rails.application.routes.draw do
  root 'upload_files#index'
  devise_for :users
  get 'upload_files/index', as: 'user_root'
  resources :upload_files, only: [:show, :create, :index, :destroy], path: '' do
    member do
      get 'download'
    end
  end
  get 'debug/delete_all_files'
  get 'debug/delete_all_users'
end
